import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;
    int Menu= 6;	
    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();

    //interagindo com usuário
	while(Menu!= 4){
    		System.out.println("Digite 1 para adicionar uma Pessoa");
    		System.out.println("2 para remover uma Pessoa");
    		System.out.println("3 para pesquisar uma Pessoa");
    		System.out.println("Ou qualquer tecla para sair");
		Menu = leitorEntrada.readLine();
		
		if(Menu == 1){
			System.out.println("Digite o nome da Pessoa:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umNome = entradaTeclado;
    			umaPessoa.setNome(umNome);

    			System.out.println("Digite o telefone da Pessoa:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umTelefone = entradaTeclado;
    			umaPessoa.setTelefone(umTelefone);

    			//adicionando uma pessoa na lista de pessoas do sistema
    			String mensagem = umControle.adicionar(umaPessoa);

    			//conferindo saída
    			System.out.println("=================================");
    			System.out.println(mensagem);
    			System.out.println("=)");
		}
		else if(Menu == 2){
			System.out.println("Digite o nome da Pessoa a ser removida");
			entradaTeclado = leitorEntrada.readline();
			String NomePessoa = entradaTeclado;
			//umaPessoa.setNome(NomePessoa);
			
			//excluindo pessoa da lista
			String mensagem = umControle.excluir(NomePessoa);
			System.out.println(mensagem);
		}
		else if(Menu == 3){
			System.out.println("Digite o nome da Pessoa: ");
			entradaTeclado= leitorEntrada.readline();
			String umNome = entradaTeclado;
			String mensagem =umControle.pesquisar(umNome);
			System.out.println(mensagem);
		}			
		else{
			Menu=4;
		}
	
			
	}
  }

}
